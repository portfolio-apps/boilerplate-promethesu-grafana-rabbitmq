#!/bin/bash

if [ -z "$1" ]
then
      echo "ERROR: First argument for NAMESPACE is empty"
      exit
fi

if [ -z "$2" ]
then
      echo "ERROR: Second argument for PASSWORD is empty"
      exit
fi

kubectl create secret docker-registry registry-credentials \
--docker-server=https://registry.gitlab.com \
--docker-username=kre8mymedia \
--docker-password=$2 \
--docker-email=kre8mymedia@gmail.com -n $1